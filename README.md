# HUZZAH FarmOS pipe
Easily configure a [Adafruit Feather HUZZAH with ESP8266 WiFi](https://www.adafruit.com/product/2821) (or similar) to send 1-wire sensor data over wifi to a [FarmOS](http://farmos.org/) [sensor](http://farmos.org/guide/assets/sensors/).




## References

### Micropython

- DHT22 sensor code: http://docs.micropython.org/en/v1.9.3/esp8266/esp8266/tutorial/dht.html
- 1-wire sensor code: http://docs.micropython.org/en/v1.9.3/esp8266/esp8266/tutorial/onewire.html
- Installing and using 'ampy' tool in order to load code onto upython boards: https://learn.adafruit.com/micropython-basics-load-files-and-run-code/install-ampy

