/*
Get sensorValue for 1wire sensor connected to a Adafruit Feather HUZZAH ESP8266 and push it to FarmOS over https

This script is based on...
 * https://gitlab.com/fiot/HUZZAH-FarmOS/blob/master/arduino/1wire-NodeRED/1wire-NodeRED.ino to get the 1wire value and post it as json
 * https://github.com/markwbrown/farmOS-ESP8266/blob/master/esp8266sensor.ino to connect to FarmOS over https
*/

#include <OneWire.h>
#include <DallasTemperature.h>

#include <ArduinoJson.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>

// Data wire is plugged into port 2 on the Arduino
#define ONE_WIRE_BUS 2

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);

char ssid[] = "yourSSID";
char pass[] = "yourWiFiPass";
String url = "https://domain.youare.at";
String listener = "/farm/sensor/listener/";
String publicKey = "sensorPublicKey";
String split = "?private_key=";
String privateKey = "sensorPrivateKey";
//to get ssl's certificate fingerprint, go to https://www.grc.com/fingerprints.htm and enter your site's address. https should be used everywhere, and you should never let facebook log you into things. 
String fingerprint = "xx xx xx xx xx xx xx xx xx xx xx xx xx xx xx xx xx xx xx xx";

void setup() {

Serial.begin(9600);

// start serial port
Serial.println("Dallas Temperature IC Control Library Demo");

// Start up the library
sensors.begin();

WiFi.begin(ssid, pass);

while (WiFi.status() != WL_CONNECTED)

delay(500);
Serial.println("Waiting for connection");

}

void loop() {

Serial.print("Requesting temperatures...");
sensors.requestTemperatures(); // Send the command to get temperatures
Serial.println("DONE");

Serial.print("Temperature for the device 1 (index 0) is: ");
int sensorValue = sensors.getTempCByIndex(0);
Serial.println(sensorValue);

// Create JSON string
StaticJsonBuffer<200> jsonBuffer;

JsonObject& root = jsonBuffer.createObject();
root["value"] = sensorValue;

String sensorJson;
root.printTo(sensorJson);


if(WiFi.status()== WL_CONNECTED){ //Check WiFi connection status

HTTPClient http;
http.begin(url+listener+publicKey+split+privateKey , fingerprint);

http.addHeader("Content-Type", "application/json");
http.addHeader("Accept", "application/json");

int httpCode = http.sendRequest("POST", sensorJson);
Serial.print("http result:");
Serial.println(httpCode);
http.writeToStream(&Serial);
http.end();

}else{
Serial.print("Error in Wifi connection");
}

delay(30000); //Send a request every 30 seconds

}
