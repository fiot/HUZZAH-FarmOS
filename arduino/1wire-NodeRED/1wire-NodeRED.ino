/**
 * BasicHTTPClient.ino
 *
 *  Created on: 24.05.2015
 *
 */


#include <OneWire.h>
#include <DallasTemperature.h>

#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

#include <ESP8266HTTPClient.h>

#include <ArduinoJson.h>

// Data wire is plugged into port 2 on the Arduino
#define ONE_WIRE_BUS 2

#define USE_SERIAL Serial

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);

ESP8266WiFiMulti WiFiMulti;

void setup() {

    USE_SERIAL.begin(115200);
   // USE_SERIAL.setDebugOutput(true);

    USE_SERIAL.println();
    USE_SERIAL.println();
    USE_SERIAL.println();

    sensors.begin();

    for(uint8_t t = 4; t > 0; t--) {
        USE_SERIAL.printf("[SETUP] WAIT %d...\n", t);
        USE_SERIAL.flush();
        delay(1000);
    }

    WiFi.mode(WIFI_STA);
    WiFiMulti.addAP("SSID", "PASSWORD");

}

void loop() {
    sensors.requestTemperatures(); // Send the command to get temperatures
    float sensorValue = sensors.getTempCByIndex(0);
    Serial.println(sensorValue); // Print out the value from the first sensor

    // Create JSON string
    StaticJsonBuffer<200> jsonBuffer;

    JsonObject& root = jsonBuffer.createObject();
    root["value"] = sensorValue;

    String sensorJson;
    root.printTo(sensorJson);
  
    // wait for WiFi connection
    if((WiFiMulti.run() == WL_CONNECTED)) {

        HTTPClient http;

        USE_SERIAL.print("[HTTP] begin...\n");
        // configure traged server and url
        //http.begin("https://192.168.1.12/test.html", "7a 9c f4 db 40 d3 62 5a 6e 21 bc 5c cc 66 c8 3e a1 45 59 38"); //HTTPS
        http.begin("http://dex.local:1880/test"); //HTTP

        http.addHeader("Content-Type", "application/json");
        http.POST(sensorJson);
        http.writeToStream(&Serial);

        http.end();
    }

    delay(10000);
}

