import ujson as json
import urequests as requests
import time
import machine
import onewire,ds18x20

# set some connection parameters

WIFI_NET = 'SSID'
WIFI_PASSWORD = 'PASSWORD'

base_url='https://test.farmos.net/farm/sensor/listener/'
public_key='PUBLIC_KEY'
private_key='PRIVATE_KEY'

url=base_url+public_key+'?private_key='+private_key
    
headers = {'Content-type':'application/json', 'Accept':'application/json'}
             
DHT22_PIN = 4

def measure_1wire():
    dat = machine.Pin(4)
    ds = ds18x20.DS18X20(onewire.OneWire(dat))
    roms = ds.scan()
    ds.convert_temp()
    time.sleep_ms(750)
    temp=ds.read_temp(roms[0])
    print(temp)
    return temp
        
def post_data(temp):
    payload={"temp": str(temp)}
    r=requests.post(url,data=json.dumps(payload),headers=headers)
    print('Status:',r.status_code)
    r.close()

def do_connect():
    import network
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        print('connecting to network...')
        sta_if.active(True)
        sta_if.connect(WIFI_NET, WIFI_PASSWORD)
        while not sta_if.isconnected():
            pass
    print('network config:', sta_if.ifconfig())

# the main bit
temp=measure_1wire()
do_connect()    
post_data(temp)
