import ujson as json
import urequests as requests
import time
import machine
import dht

# set some connection parameters

WIFI_NET = 'SSID'
WIFI_PASSWORD = 'PASSWORD'

base_url='https://test.farmos.net/farm/sensor/listener/'
public_key='PUBLIC_KEY'
private_key='PRIVATE_KEY'

url=base_url+public_key+'?private_key='+private_key
    
headers = {'Content-type':'application/json', 'Accept':'application/json'}
             
DHT22_PIN = 4

def measure_dht():
    d=dht.DHT22(machine.Pin(DHT22_PIN))
    d.measure()
    time.sleep(1) #allow delay for measurement
    temp = d.temperature()
    humidity = d.humidity()
    print temp,humidity
    return temp,humidity
    
def post_data(temp,humidity):
    payload={"temp": str(temp),"humidity": str(humidity)}
    r=requests.post(url,data=json.dumps(payload),headers=headers)
    print('Status:',r.status_code)
    r.close()

def do_connect():
    import network
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        print('connecting to network...')
        sta_if.active(True)
        sta_if.connect(WIFI_NET, WIFI_PASSWORD)
        while not sta_if.isconnected():
            pass
    print('network config:', sta_if.ifconfig())

# the main bit
temp,humidity=measure_dht()
do_connect()    
post_data(temp,humidity)
